import Joi from "joi"
import { DELETE_STRATEGY } from "../constants/loan_application";
import { AppResponse } from "../types/app_response";
import { parseJSONString } from "../utiliities/general";

const validateCreateLoanApplicationRequest = (event: Record<any, any>): AppResponse => {
  const schema = Joi.object({
    amount: Joi.number().min(0).required(),
    companyId: Joi.string().required(),
  });

  const pathParams = event.pathParameters || {};
  const body = parseJSONString(event.body) || {};
  const queryParams = event.queryParameters || {};

  const { error } = schema.validate({ ...pathParams, ...body, ...queryParams });
  if (error) {
    return {
      success: false,
      error: error.message,
    }
  }
  return {
    success: true,
  }
}


const validateDisburseLoanApplicationRequest = (event: Record<any, any>): AppResponse => {
  const schema = Joi.object({
    id: Joi.string().required(),
  });

  const pathParams = event.pathParameters || {};

  const { error } = schema.validate({ ...pathParams, });
  if (error) {
    return {
      success: false,
      error: error.message,
    }
  }
  return {
    success: true,
  }
}

const validateDeleteLoanApplicationRequest = (event: Record<any, any>): AppResponse => {
  const schema = Joi.object({
    id: Joi.string().required(),
    nature: Joi.string().valid(DELETE_STRATEGY.HARD, DELETE_STRATEGY.SOFT).allow(null, "")
  });

  const pathParams = event.pathParameters || {};

  const { error } = schema.validate({ ...pathParams, });
  if (error) {
    return {
      success: false,
      error: error.message,
    }
  }
  return {
    success: true,
  }
}


const validateUpdateLoanDisbursedStatusRequest = (event: Record<any, any>): AppResponse => {
  const apiKeyField = "x-api-key";
  const schema = Joi.object({
    id: Joi.string().required(),
    [apiKeyField]: Joi.string().required(),
  });

  const pathParams = event.pathParameters || {};
  const apiKey = event.headers["x-api-key"] || "";

  const { error } = schema.validate({ ...pathParams, ...{ [apiKeyField]: apiKey } });
  if (error) {
    return {
      success: false,
      error: error.message,
    }
  }
  return {
    success: true,
  }
}


export {
  validateCreateLoanApplicationRequest,
  validateDisburseLoanApplicationRequest,
  validateDeleteLoanApplicationRequest,
  validateUpdateLoanDisbursedStatusRequest,
}
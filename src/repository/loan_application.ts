import { STATUS_CODES } from "../constants/errors";
import { DELETE_STRATEGY, LOAN_APPLICATION_STATUS, } from "../constants/loan_application";
import logger, { ErrorInfo } from "../dependencies/logger";
import Loan, { createTables } from "../models/loan_application"
import { AppResponse } from "../types/app_response";
import { LoanInfo } from "../types/response_dto";

/** createLoan accepts laon data and created the loan record in the data store. If it encounters an error the error is logged and 
 * a server error response containing the error detail is issued to the upstream caller
 */
const createLoan = async (amount: number, companyId: string, companyDetails: any): Promise<AppResponse> => {
  try {
    const loan = {
      amount,
      companyId,
      companyInformation: companyDetails,
      status: LOAN_APPLICATION_STATUS.OFFERED,
      deleted: false
    }
    const laonObj = await new Loan(loan);
    await createTables();
    const loanData = await laonObj.save();

    return {
      success: true,
      data: loanData?.attrs,
    }
  } catch (exception) {
    const errorMessage = `failed to create loan for company with companyId: ${companyId} \n ${exception}`
    const err = new ErrorInfo(
      "createLoan",
      errorMessage,
      String(STATUS_CODES.SERVER_ERR),
    )
    logger.error(err.formatError());
    return {
      success: false,
      error: errorMessage,
      code: (STATUS_CODES.SERVER_ERR)
    }
  }
}

/** getLoanById retrieves laon record from the data store. If it encounters an error the error is logged and 
 * a server error response containing the error detail is issued to the upstream caller
 */
const getLoanById = async (loanId: string): Promise<AppResponse> => {
  try {
    const loanData = await Loan.get(loanId);
    return {
      success: true,
      data: loanData?.attrs || null,
    }
  } catch (exception) {
    const errorMessage = `failed to load loan with companyId of: ${loanId} \n ${exception}`
    const err = new ErrorInfo(
      "getLoanById",
      errorMessage,
      String(STATUS_CODES.SERVER_ERR),
    )
    logger.error(err.formatError());
    return {
      success: false,
      error: errorMessage
    }
  }
}


/** updateLoanStatusById updates laon record in the data store with a valid loan status.
 *  If it encounters an error the error is logged and  a server error response containing the 
 * error detail is issued to the upstream caller
 */
const updateLoanStatusById = async (loanId: string, status: string): Promise<AppResponse> => {
  const loan = {
    id: loanId,
    status,
  }
  try {
    const loanData = await Loan.update(loan, {ReturnValues: 'ALL_NEW'});
    return {
      success: true,
      data: loanData.attrs,
    }
  } catch (exception) {
    const errorMessage = `failed to update loan with id of: ${loanId} to status: ${status} \n ${exception}`
    const err = new ErrorInfo(
      "updateLoanStatusById",
      errorMessage,
      String(STATUS_CODES.SERVER_ERR),
    )
    logger.error(err.formatError());
    return {
      success: false,
      error: errorMessage,
      code: STATUS_CODES.SERVER_ERR
    }
  }
}

/** getAllLoans retrieves all laon records 100 at a time from the data store and returns them to the caller.
 *  If it encounters an error the error is logged and  a server error response containing the 
 * error detail is issued to the upstream caller
 */
const getAllLoans = async (): Promise<AppResponse> => {
  try {

    const stream = await Loan.scan().limit(100).loadAll().exec();
    const data: LoanInfo[] = [];
    for await (const laonInfo of stream) {
      data.push(laonInfo.Items);
    }
    return {
      success: true,
      data: data,
    }

  } catch (exception) {
    const errorMessage = `failed to get all loans \n ${exception}`
    const err = new ErrorInfo(
      "getAllLoans",
      errorMessage,
      String(STATUS_CODES.SERVER_ERR),
    )
    logger.error(err.formatError());
    return {
      success: false,
      error: errorMessage
    }
  }
}

/** deleteLoanById deletes a laon record in the data store and returns them to the caller. If the delete nature
 * is not specified, it performs a soft delete where the record is set as deleted  = true and if it is specified
 * as hard delete then the record is completely removed fromt the data store. Soft is the default strategy
 *  If it encounters an error the error is logged and  a server error response containing the 
 * error detail is issued to the upstream caller
 */
const deleteLoanById = async (loanId: string, nature: string = DELETE_STRATEGY.SOFT): Promise<AppResponse> => {
  const loan = {
    id: loanId,
    deleted: true,
  }
  try {
    let loanData: { attrs: any; };
    if (nature == DELETE_STRATEGY.SOFT) {
      loanData = await Loan.update(loan);
    } else {
      loanData = await Loan.destroy(loan, { ReturnValues: 'ALL_OLD' });
    }
    return {
      success: true,
      data: loanData?.attrs
    }
  } catch (exception) {
    const errorMessage = `failed to delete loans with id: ${loanId} \n ${exception}`
    const err = new ErrorInfo(
      "deleteLoanById",
      errorMessage,
      String(STATUS_CODES.SERVER_ERR),
    )
    logger.error(err.formatError());
    return {
      success: false,
      error: errorMessage,
      code: STATUS_CODES.SERVER_ERR
    }
  }
}

export {
  createLoan,
  getLoanById,
  getAllLoans,
  deleteLoanById,
  updateLoanStatusById,
}
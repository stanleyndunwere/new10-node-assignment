import { STATUS_CODES } from "../constants/errors";
import logger, { ErrorInfo } from "../dependencies/logger";
import { setLoanStatusToDisbursed } from "../helpers/loan_application";
import { HttpResponse } from "../types/app_response";
import { validateUpdateLoanDisbursedStatusRequest } from "../validations/loan_application";


/**  updateLoanStatusToDisbursed This function accepts and validates a request to disburse 
 * a loan with a header for security validation and the loan id for marking loan as disbursed */
const updateLoanStatusToDisbursed = async (event): Promise<HttpResponse> => {
	try {
		/* perform validation*/
		const validation = validateUpdateLoanDisbursedStatusRequest(event);
		if (!validation.success) {
			return {
				statusCode: STATUS_CODES.BAD_REQUEST,
				body: {
					error: validation.error,
					message: "validation failed"
				}
			}
		}

		const { id } = event.pathParameters;

		/** perform loan disbursement if validation is successful */
		const disburseLoan = await setLoanStatusToDisbursed(id);

		if (!disburseLoan.success) {
			return {
				statusCode: disburseLoan.code || STATUS_CODES.BAD_REQUEST,
				body: {
					message: "failed to disburse loan application",
					error: disburseLoan.error
				}
			}
		}

		return {
			statusCode: disburseLoan.code || STATUS_CODES.OK,
			body: {
				message: "Successfully disbursed loan application",
				data: disburseLoan.data,
			}
		}

	} catch (exception) {
		const errorMessage = `something went wrong while processing your request \n ${exception}`
		const err = new ErrorInfo(
			"updateLoanStatusToDisbursed",
			errorMessage,
			String(STATUS_CODES.SERVER_ERR),
		)
		logger.error(err.formatError());
		return {
			statusCode: STATUS_CODES.SERVER_ERR,
			body: {
				error: "server error",
				message: "something went wrong while processing your request"
			}
		}
	}
}

export {
	updateLoanStatusToDisbursed,
}
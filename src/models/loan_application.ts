import { dynamoClient } from "../dependencies/dynamo_db";
import { LOAN_APPLICATION_STATUS } from "../constants/loan_application";
import Joi from "joi";
import logger, { ErrorInfo } from "../dependencies/logger";
import { STATUS_CODES } from "../constants/errors";


const dynamo = dynamoClient();
const Loan = dynamo.define('Loan', {
  hashKey: "id",
  timestamps: true,
  schema: {
    id: dynamo.types.uuid(),
    amount: Joi.number().required(),
    companyId: Joi.string().allow(null, ""),
    status: Joi.string().valid(LOAN_APPLICATION_STATUS.OFFERED, LOAN_APPLICATION_STATUS.DISBURSED).required(),
    companyInformation: Joi.object(),
    deleted: Joi.boolean().default(false),
  },
});

/** createTables creates a model for the loan record in the data store */
const createTables = async () => {
  try {
    await dynamo.createTables();
  } catch (exception) {
    const errorMessage = `failed to create table: loan in dynamodb \n ${exception}`
    const err = new ErrorInfo(
      "loanModel",
      errorMessage,
      String(STATUS_CODES.SERVER_ERR),
    )
    logger.error(err.formatError());
    return {
      success: false,
      error: errorMessage,
      code: (STATUS_CODES.SERVER_ERR)
    }
  }
}

export default Loan;
export {
  createTables,
}
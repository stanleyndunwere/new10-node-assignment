import config from "../config/config.json";
import { createLoanApplication, deleteLoanApplication, disburseLoan, getAllLoanApplications } from "../app1/functions";
import { updateLoanStatusToDisbursed } from "../app2/functions";
import { STATUS_CODES } from "../constants/errors";
import { API_KEY_FIELD, DELETE_STRATEGY, LOAN_APPLICATION_STATUS } from "../constants/loan_application";
import { createEvent, getTestHttpClient } from "./test_utils";
import { deleteLoanById } from "../repository/loan_application";

describe('should test all the end points and handlers in api1 and api2', () => {
  const activeCompanyId = "rechtspersoon-37118328-de-goede-inspecties-bv";
  const inactiveCompanyId = "hoofdvestiging-24363638-0000-nagelsalon-divas-heaven";
  const amount = 400;

  const createdLoanIds: string[] = [];
  const softDeletedLoanIds: string[] = [];
  const hardDeletedLoanIds: string[] = [];
  const disbursedLoanIds: string[] = [];

  beforeAll(async () => {
    // jest.setTimeout(newTimeout)
    const urlParams = {
      companyId: activeCompanyId,
      amount,
    }
    const event = createEvent(urlParams, "POST");
    /**insert two dummy loan records into the datastore */
    for (let i = 0; i < 5; i++) {
      const resp = await createLoanApplication(event);
      createdLoanIds.push(resp?.body?.data?.id || null);
    }
  });

  afterAll(async () => {
    const allIds = [
      ...createdLoanIds,
      ...softDeletedLoanIds,
      ...hardDeletedLoanIds,
      ...disbursedLoanIds,
    ];

    for (const id of allIds) {
      await deleteLoanById(id, DELETE_STRATEGY.HARD);
    }
  });

  describe('app1 lambdas', () => {
    it('should make a request to create a new loan with active company and return a success response', async () => {
      const urlParams = {
        companyId: activeCompanyId,
        amount,
      }
      const event = createEvent(urlParams, "POST");
      const resp = await createLoanApplication(event);
      createdLoanIds.push(resp?.body?.data?.id || null);

      expect(resp?.statusCode).toBe(STATUS_CODES.CREATED);
      expect(resp.body?.data?.amount).toEqual(amount);
      expect(resp.body?.data?.companyId).toBe(activeCompanyId);
    });
  });

  describe('app1 lambdas', () => {
    it('should make a request to create a new loan with inactive company and return a failure response', async () => {
      const urlParams = {
        companyId: inactiveCompanyId,
        amount,
      }
      const event = createEvent(urlParams, "POST");
      const resp = await createLoanApplication(event);

      expect(resp?.statusCode).toBe(STATUS_CODES.FAILED_PRECONDITION);
    });
  });

  describe('app1 lambdas', () => {
    it('should make a request to create a new loan with incomplete data and fail validation', async () => {
      const urlParams = {
        companyId: inactiveCompanyId,
      }
      const event = createEvent(urlParams, "POST");
      const resp = await createLoanApplication(event);

      expect(resp?.statusCode).toBe(STATUS_CODES.BAD_REQUEST);
    });
  });

  describe('app1 lambdas', () => {
    it('should make a request to fetch all loan applications and return successful response', async () => {
      const resp = await getAllLoanApplications({});
      expect(resp?.statusCode).toBe(STATUS_CODES.OK);
      expect(resp?.body?.data).not.toBeNull();
    });
  });

  describe('app1 lambdas', () => {
    it('should attempt to soft delete loan application record successfully', async () => {
      const id = createdLoanIds.shift();
      const urlParams1 = {
        id,
      }
      const event = createEvent(urlParams1, "DELETE");
      const resp = await deleteLoanApplication(event);
      softDeletedLoanIds.push(resp?.body?.data?.id);
      expect(resp?.statusCode).toBe(STATUS_CODES.OK);
    });
  });

  describe('app1 lambdas', () => {
    it('should attempt to hard delete loan application record successfully', async () => {
      const id = createdLoanIds.shift();
      const urlParams1 = {
        id,
        nature: DELETE_STRATEGY.HARD,
      };
      const event = createEvent(urlParams1, "DELETE");
      const resp = await deleteLoanApplication(event);
      hardDeletedLoanIds.push(id!);
      expect(resp?.statusCode).toBe(STATUS_CODES.OK);
    });
  });

  describe('app1 lambdas', () => {
    it('should attempt to delete loan application without required params and return unsuccessful', async () => {
      const urlParams = {};
      const event = createEvent(urlParams, "DELETE");
      const resp = await deleteLoanApplication(event);
      expect(resp?.statusCode).toBe(STATUS_CODES.BAD_REQUEST);
    });
  });

  describe('app2 lambdas', () => {
    it('should attempt to disburse loan without header loan application record unsuccessfully', async () => {
      const urlParams = {
        id: createdLoanIds.shift()
      };
      const event = createEvent(urlParams, "PUT");
      const resp = await updateLoanStatusToDisbursed(event);
      expect(resp?.statusCode).toBe(STATUS_CODES.BAD_REQUEST);
      expect(resp?.body?.error).toBe('"x-api-key" is not allowed to be empty');
    });
  });

  describe('app2 lambdas', () => {
    it('should attempt to disburse soft deleted loan application record unsuccessfully', async () => {
      const urlParams = {
        id: softDeletedLoanIds.shift()
      };
      const headers = { [API_KEY_FIELD]: config.api1Apikey };
      const event = createEvent(urlParams, "PUT", headers);
      const resp = await updateLoanStatusToDisbursed(event);
      expect(resp?.statusCode).toBe(STATUS_CODES.NOT_FOUND);
      expect(resp?.body?.error).toBe(`loan information not found for loan with id: ${urlParams.id}`);
    });
  });

  describe('app2 lambdas', () => {
    it('should attempt to disburse loan application record successfully', async () => {
      const urlParams1 = {
        id: createdLoanIds.shift()
      };
      const headers = { [API_KEY_FIELD]: config.api1Apikey };
      const event = createEvent(urlParams1, "PUT", headers);
      const resp = await updateLoanStatusToDisbursed(event);
      disbursedLoanIds.push(urlParams1.id!);
      expect(resp?.statusCode).toBe(STATUS_CODES.OK);
      expect(resp?.body?.data?.status).toBe(LOAN_APPLICATION_STATUS.DISBURSED);
    });
  });

  describe('app2 lambdas', () => {
    it('should attempt to disburse loan application record that is already disbursed and return not modified response', async () => {
      const urlParams = {
        id: disbursedLoanIds.shift()
      };
      const headers = { [API_KEY_FIELD]: config.api1Apikey };
      const event = createEvent(urlParams, "PUT", headers);
      const resp = await updateLoanStatusToDisbursed(event);
      disbursedLoanIds.push(urlParams.id!);
      expect(resp?.statusCode).toBe(STATUS_CODES.NOT_MODIFIED);
      expect(resp?.body?.data?.message).toBe(`loan with loan Id: ${urlParams.id} already disbursed`);
    });
  });

});



import config from "../config/config.json";
import HttpClient from "../dependencies/http_client";
import { getAPI2HttpClient, getCompanyValidatorHttpClient } from "../helpers/init_http_clients"

const getTestHttpApi2Client = () => {
  return getAPI2HttpClient();
}

const getTestCompanyValidatorClient = () => {
  return getCompanyValidatorHttpClient();
}

const getTestHttpClient = () => {
  const api1BaseUrl = "http://localhost:3000/dev/";
  const headers = {
    "Accept": "application/json",
    "Content-Type": "application/json; charset=utf-8",
  };

  return new HttpClient(headers, api1BaseUrl);
}


const createEvent = (urlParams: any, method: string, headers?: Record<any, any> | null) => {
  return {
    body: null,
    headers: {
      'Content-Type': 'application/json',
      'User-Agent': 'PostmanRuntime/7.29.0',
      Accept: '*/*',
      Host: 'localhost:3000',
      'Accept-Encoding': 'gzip, deflate, br',
      Connection: 'keep-alive',
      ...headers
    },
    httpMethod: method.toUpperCase(),
    isBase64Encoded: false,
    multiValueHeaders: {
      'Content-Type': ['application/json'],
      Accept: ['*/*'],
      Host: ['localhost:3000'],
      'Accept-Encoding': ['gzip, deflate, br'],
      Connection: ['keep-alive'],
    },
    multiValueQueryStringParameters: null,
    path: '',
    pathParameters: urlParams,
    queryStringParameters: null,
    requestContext: {
      accountId: 'offlineContext_accountId',
      apiId: 'offlineContext_apiId',
      authorizer: {
        claims: undefined,
        scopes: undefined,
        principalId: 'offlineContext_authorizer_principalId'
      },
      domainName: 'offlineContext_domainName',
      domainPrefix: 'offlineContext_domainPrefix',
      extendedRequestId: 'cl1mggrqd0000elpg1i5pglas',
      httpMethod: method.toUpperCase(),
      identity: {
        accessKey: null,
        accountId: 'offlineContext_accountId',
        apiKey: 'offlineContext_apiKey',
        apiKeyId: 'offlineContext_apiKeyId',
        caller: 'offlineContext_caller',
        cognitoAuthenticationProvider: 'offlineContext_cognitoAuthenticationProvider',
        cognitoAuthenticationType: 'offlineContext_cognitoAuthenticationType',
        cognitoIdentityId: 'offlineContext_cognitoIdentityId',
        cognitoIdentityPoolId: 'offlineContext_cognitoIdentityPoolId',
        principalOrgId: null,
        sourceIp: '127.0.0.1',
        user: 'offlineContext_user',
        userAgent: 'PostmanRuntime/7.29.0',
        userArn: 'offlineContext_userArn'
      },
      operationName: undefined,
      path: '/create/40/hoofdvestiging-24363638-0000-nagelsalon-divas-heaven',
      protocol: 'HTTP/1.1',
      requestId: 'cl1mggrqd0001elpga5uvduaf',
      requestTime: '05/Apr/2022:19:09:00 +0100',
      requestTimeEpoch: Date.parse(new Date().toString()),
      resourceId: 'offlineContext_resourceId',
      resourcePath: '/dev/create/{amount}/{companyId}',
      stage: 'dev'
    },
    resource: '',
    stageVariables: undefined
  };

}
export {
  getTestHttpApi2Client,
  getTestCompanyValidatorClient,
  getTestHttpClient,
  createEvent,
}
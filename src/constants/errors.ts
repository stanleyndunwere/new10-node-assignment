// eslint-disable-next-line prettier/prettier
export const STATUS_CODES = {
  SERVER_ERR: 500,
  NOT_FOUND: 404,
  CONFLICT: 409,
  BAD_REQUEST: 400,
  UNAUTHORISED: 401,
  CREATED: 201,
  OK: 200,
  ACCEPTED: 202,
  NOT_MODIFIED: 304,
  FAILED_PRECONDITION: 412,
}


/* eslint-disable prettier/prettier */
export const LOAN_APPLICATION_STATUS = {
  OFFERED: 'offered',
  DISBURSED: 'disbursed',
  APPLIED: 'applied',
}

export const DEFAULT_LIMIT = 100;

export const COMPANY_INFO_API_BASE_URL = "https://api.overheid.io/openkvk/";

export const DELETE_STRATEGY = {
  SOFT: "soft",
  HARD: "hard",
}

export const API_KEY_FIELD = "x-api-key";
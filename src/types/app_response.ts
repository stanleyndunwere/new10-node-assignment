
type HttpResponse = {
  statusCode: number,
  body?: string | any,
}

type AppResponse = {
  success: boolean,
  data?: any | null,
  error?: string,
  code?: number,
}

export {
  AppResponse,
  HttpResponse,
}
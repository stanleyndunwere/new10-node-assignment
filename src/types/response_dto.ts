
type CompanyInfo = {
  website: null;
  plaats: string;
  datum_oprichting: string;
  postcode: string;
  dossiernummer: string;
  type: string;
  non_mailing_indicatie: boolean;
  omschrijving: string;
  subdossiernummer: string;
  updated_at: string;
  straat: string;
  huisnummer: string,
  actief: boolean,
  bestaandehandelsnaam: string[];
  sbi: number[];
  soort_onderneming: string;
  handelsnaam: string;
  _links: {
    self: {
      href: string;
    }
  }
}

type LoanInfo = {
  createdAt: string;
  amount: number,
  companyId: string,
  deleted: boolean,
  id: string,
  status: string,
  companyInfo: CompanyInfo
};

export {
  LoanInfo,
  CompanyInfo,
}
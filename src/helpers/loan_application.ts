import { AppResponse } from "../types/app_response";
import logger, { ErrorInfo } from "../dependencies/logger";
import { STATUS_CODES } from "../constants/errors";
import { CompanyInfo } from "../types/response_dto";
import { createLoan, getLoanById, updateLoanStatusById } from "../repository/loan_application";
import { LOAN_APPLICATION_STATUS } from "../constants/loan_application";
import { getAPI2HttpClient, getCompanyValidatorHttpClient } from "./init_http_clients";

/** updateDisbursedStatus makes a http call to API2 service to update loan status to disbursed */
const updateDisbursedStatus = async (loanId: string): Promise<AppResponse> => {
  try {
    const response = await getAPI2HttpClient().put<CompanyInfo>(`/${loanId}`, {});

    return {
      success: true,
      data: response.data
    }
  } catch (exception) {
    console.log(exception.response)
    const errorMessage = `failed to complete action to update loan to disbursed for loan with id ${loanId}} \n ${exception}`
    const err = new ErrorInfo(
      "updateDisbursedStatus",
      errorMessage,
      String(STATUS_CODES.SERVER_ERR),
    )
    logger.error(err.formatError());
    return {
      success: false,
      error: exception.response.data.error,
      code: exception.response.status,
    }
  }
}

/** fetchCompanyDetailsById fetches company details using the company id from the openkvk service */
const fetchCompanyDetailsById = async (companyId: string): Promise<AppResponse> => {
  try {
    const response = await getCompanyValidatorHttpClient().get<CompanyInfo>(`/${companyId}`);
    return {
      success: true,
      data: response.data
    }
  } catch (exception) {
    const errorMessage = `failed to fetch company informaton from API with id: ${companyId} \n ${exception}`
    const err = new ErrorInfo(
      "fetchCompanyLoanDetailsById",
      errorMessage,
      String(STATUS_CODES.SERVER_ERR),
    )
    logger.error(err.formatError());
    return {
      success: false,
      error: errorMessage,
      code: exception.response.status,
    }
  }
}

/** createLoanRequest creates a loan request for a business inside of the data store
 *  after validating that company exists and is currently active via openkvk service
 */
const createLoanRequest = async (amount: number, companyId: string): Promise<AppResponse> => {
  const resp = await fetchCompanyDetailsById(companyId);
  if (!resp.success) {
    return resp;
  }

  const companyInfo = resp.data as CompanyInfo;
  if (!companyInfo.actief) {
    return {
      success: false,
      error: `company with Id: ${companyId} is currently inactive hence loan cannot be processed`,
      code: STATUS_CODES.FAILED_PRECONDITION,
    }
  }

  const created = await createLoan(amount, companyId, companyInfo);
  if (!created.success) {
    return created
  }

  return {
    success: true,
    data: created.data,
    code: STATUS_CODES.CREATED
  }
}

/** setLoanStatusToDisbursed validates that a loan is valid and the company attached to the laon is active
 * then marks the loan as disbursed. If the loan has a deleted flag set as true, or the loan record cannot be found, 
 * the function returns a not found error to the caller
*/
const setLoanStatusToDisbursed = async (loanId: string): Promise<AppResponse> => {
  const loanInfo = await getLoanById(loanId);
  if (!loanInfo.success) {
    return loanInfo
  }

  if (loanInfo.data?.deleted) {
    return {
      success: false,
      error: "loan information not found for loan with id: " + loanId,
      code: STATUS_CODES.NOT_FOUND
    }
  }

  if (loanInfo.data?.status === LOAN_APPLICATION_STATUS.DISBURSED) {
    return {
      success: true,
      code: STATUS_CODES.NOT_MODIFIED,
      data: { message: "loan with loan Id: " + loanId + " already disbursed" },
    }
  }

  const companyId = loanInfo.data?.companyId;
  if (!companyId) {
    return {
      success: false,
      error: "could not find company id for loan with loanid: " + loanId,
      code: STATUS_CODES.NOT_FOUND,
    }
  }

  const resp = await fetchCompanyDetailsById(companyId);
  if (!resp.success) {
    return resp;
  }

  const companyInfo = resp.data as CompanyInfo;
  if (!companyInfo.actief) {
    return {
      success: false,
      error: `company with Id: ${companyId} is currently inactive hence loan cannot be processed`,
      code: STATUS_CODES.FAILED_PRECONDITION,
    }
  }

  const disburse = await updateLoanStatusById(loanId, LOAN_APPLICATION_STATUS.DISBURSED);
  if (!disburse.success) {
    return disburse
  }

  return {
    success: true,
    data: disburse.data,
  }
}

export {
  fetchCompanyDetailsById,
  createLoanRequest,
  setLoanStatusToDisbursed,
  updateDisbursedStatus,
}



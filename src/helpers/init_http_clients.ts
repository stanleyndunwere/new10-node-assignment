import config from "../config/config.json";
import { API_KEY_FIELD, COMPANY_INFO_API_BASE_URL } from "../constants/loan_application";
import HttpClient from "../dependencies/http_client";

/** getAPI2HttpClient initializes and configures http client for making external api calls to API2 service */
const getAPI2HttpClient = (): HttpClient => {
  const headers = {
    "Accept": "application/json",
    "Content-Type": "application/json; charset=utf-8",
    [API_KEY_FIELD]: config.api1Apikey,
  };

  return new HttpClient(headers, config.api2BaseUrl);
}

/** getCompanyValidatorHttpClient initializes and configures http client for making external api calls to openkvk service */
const getCompanyValidatorHttpClient = (): HttpClient => {
  const headers = {
    "Accept": "application/json",
    "Content-Type": "application/json; charset=utf-8",
    "ovio-api-key": config.companyInfoApiKey,
  };

  return new HttpClient(headers, COMPANY_INFO_API_BASE_URL);
}

export {
  getAPI2HttpClient,
  getCompanyValidatorHttpClient,
}
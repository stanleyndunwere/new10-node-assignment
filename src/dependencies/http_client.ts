import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";

/** HttpClient: is a wrapper class that initializes and returns an axios http client configured with headers and base url */
class HttpClient {
  private client: AxiosInstance;
  constructor(headers: Record<string, string | boolean>, baseUrl: string) {
    this.client = axios.create({
      baseURL: baseUrl,
      timeout: 3000,
      headers: headers
    });
  }

  /** get: is a wrapper function for performing get requests using axios get  */
  public async get<D>(url: string): Promise<AxiosResponse> {
    return this.client.get(url);
  }

  /** put: is a wrapper function for performing put requests using axios put  */
  public async put<D>(url: string, body: Record<any, any>): Promise<AxiosResponse> {
    return this.client.put(url, body);
  }
}

export default HttpClient;
import winston from "winston";

/** logger initializes a winston logger instance for logging errors and messages in the codebase */
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.File({ filename: 'app_error.log', level: 'debug' }),
    new winston.transports.File({ filename: 'combined.log' }),
  ],
});

/** ErrorInfo custom class to handle and produce consistent unified error logging.
 *  Mandates specification of errorCode, errorMessage and the function
 * name where error was encountered
 */
class ErrorInfo {
  private errorFuntionName: string;
  private errorMessage: string;
  private errorCode: string;

  constructor(errorFunc: string, errorMessage: string, errorCode: string) {
    this.errorFuntionName = errorFunc;
    this.errorMessage = errorMessage;
    this.errorCode = errorCode;
  }

  /** formatError convert the custom error object into a json string */
  formatError() {
    return JSON.stringify(this, null, 2);
  }
}

export default logger;
export { ErrorInfo }
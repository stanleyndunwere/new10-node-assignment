
const dynamo = require('dynamodb')
import config from '../config/config.json';

/**  dynamoClient configures, initializes and returns a dynamo db client */
const dynamoClient = () => {
  const credentials = new dynamo.AWS.Credentials('akid', 'secret', 'session');
  dynamo.AWS.config.update({
    region: config.dynamoRegion,
    endpoint: config.dynamoEndpoint,
    credentials,
  });
  return dynamo;
};

export {
  dynamoClient
}
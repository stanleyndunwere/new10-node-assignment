import logger from "../dependencies/logger"

/** parseJSONString attempts to parse a JSON string and return a valid json value. If it encounters an error
 * the error is logged and null is returned
 */
const parseJSONString = (value: string): Record<any, any> | null => {
  try {
    return JSON.parse(value)
  } catch (exception) {
    logger.error(`failed to parse json string: ${value}. \n ${exception}`);
    return null;
  }
}

export {
  parseJSONString
}
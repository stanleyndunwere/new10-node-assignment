/* eslint-disable prettier/prettier */
import config from '../config/config.json'
import { updateLoanStatusToDisbursed } from '../app2/functions'
import { STATUS_CODES } from '../constants/errors'
import logger, { ErrorInfo } from '../dependencies/logger'
import { createLoanRequest, updateDisbursedStatus } from '../helpers/loan_application'
import { deleteLoanById, getAllLoans, } from '../repository/loan_application'
import { HttpResponse } from '../types/app_response'
import {
	validateCreateLoanApplicationRequest,
	validateDeleteLoanApplicationRequest,
	validateDisburseLoanApplicationRequest
} from '../validations/loan_application'

/** createLoanApplication accepts a loan amount and a customer id and performs validations before creating 
 * loan application records in the data store
*/
const createLoanApplication = async (event): Promise<HttpResponse> => {

	try {
		/**perform validation of request information */
		const validation = validateCreateLoanApplicationRequest(event);
		if (!validation.success) {
			return {
				statusCode: STATUS_CODES.BAD_REQUEST,
				body: {
					error: validation.error,
					message: "validation failed"
				}
			}
		}

		const { amount, companyId } = event.pathParameters;
		/**attempt create loan call */
		const createLoan = await createLoanRequest(amount, companyId);

		if (!createLoan.success) {
			return {
				statusCode: createLoan.code || STATUS_CODES.BAD_REQUEST,
				body: {
					message: "failed to create loan application",
					error: createLoan.error
				}
			}
		}

		return {
			statusCode: STATUS_CODES.CREATED,
			body: {
				message: "Successfully created loan application",
				data: createLoan.data,
			}
		}
	} catch (exception) {
		const errorMessage = `something went wrong while processing your request \n ${exception}`
		const err = new ErrorInfo(
			"createLoanApplication",
			errorMessage,
			String(STATUS_CODES.SERVER_ERR),
		)
		logger.error(err.formatError());
		return {
			statusCode: STATUS_CODES.SERVER_ERR,
			body: {
				error: "server error",
				message: "something went wrong while processing your request"
			}
		}
	}
}

/** getAllLoanApplications retrieves all loan records in the company */
const getAllLoanApplications = async (event): Promise<HttpResponse> => {
	try {
		const getLoans = await getAllLoans();
		if (!getLoans.success) {
			return {
				statusCode: getLoans.code || STATUS_CODES.BAD_REQUEST,
				body: {
					message: "failed to fetch all loan applications",
					error: getLoans.error
				}
			}
		}

		return {
			statusCode: STATUS_CODES.OK,
			body: {
				message: "Successfully fetched all loan applications",
				data: getLoans.data,
			}
		}
	} catch (exception) {
		const errorMessage = `something went wrong while processing your request \n ${exception}`
		const err = new ErrorInfo(
			"getAllLoanApplications",
			errorMessage,
			String(STATUS_CODES.SERVER_ERR),
		)
		logger.error(err.formatError());
		return {
			statusCode: STATUS_CODES.SERVER_ERR,
			body: {
				error: "server error",
				message: "something went wrong while processing your request"
			}
		}
	}

}

/** deleteLoanApplication accepts a loan identifier and sets the status of the loan to deleted if marked as soft delete 
 * or removes the loan record entirely from the database if specified as hard delete
	*/
const deleteLoanApplication = async (event): Promise<HttpResponse> => {

	try {
		const validation = validateDeleteLoanApplicationRequest(event);

		if (!validation.success) {
			return {
				statusCode: STATUS_CODES.BAD_REQUEST,
				body: {
					error: validation.error,
					message: "validation failed"
				}
			}
		}

		const { id, nature } = event.pathParameters;
		const deleteLoan = await deleteLoanById(id, nature);

		if (!deleteLoan.success) {
			return {
				statusCode: deleteLoan.code || STATUS_CODES.BAD_REQUEST,
				body: {
					message: "failed to delete loan application",
					error: deleteLoan.error
				}
			}
		}

		return {
			statusCode: STATUS_CODES.OK,
			body: {
				message: "Successfully deleted loan application",
				data: deleteLoan.data,
			}
		}
	} catch (exception) {
		const errorMessage = `something went wrong while processing your request \n ${exception}`
		const err = new ErrorInfo(
			"deleteLoanApplication",
			errorMessage,
			String(STATUS_CODES.SERVER_ERR),
		)
		logger.error(err.formatError());
		return {
			statusCode: STATUS_CODES.SERVER_ERR,
			body: {
				error: "server error",
				message: "something went wrong while processing your request"
			}
		}
	}
}

/** disburseLoan attempts an api call to the API2 service and attempts to disburse a loan 
 * if successful the loan is marked as disbursed else an error response is returned
 */
const disburseLoan = async (event): Promise<HttpResponse> => {
	try {
		const validation = validateDisburseLoanApplicationRequest(event);
		if (!validation.success) {
			return {
				statusCode: STATUS_CODES.BAD_REQUEST,
				body: {
					error: validation.error,
					message: "validation failed"
				}
			}
		}

		const { id } = event.pathParameters;
		/** make disburse request to second service (Api2) */
		const makeRequest = await updateDisbursedStatus(id);
		if (!makeRequest.success) {
			return {
				statusCode: makeRequest.code || STATUS_CODES.BAD_REQUEST,
				body: {
					message: "failed to make disburse request",
					error: makeRequest.error
				}
			}
		}

		return {
			statusCode: STATUS_CODES.OK,
			body: {
				message: "Successfully deleted loan application",
				data: makeRequest.data,
			}
		}
	} catch (exception) {
		const errorMessage = `something went wrong while processing your request \n ${exception}`
		const err = new ErrorInfo(
			"disburseLoan",
			errorMessage,
			String(STATUS_CODES.SERVER_ERR),
		)
		logger.error(err.formatError());
		return {
			statusCode: STATUS_CODES.SERVER_ERR,
			body: {
				error: "server error",
				message: "something went wrong while processing your request"
			}
		}
	}

}

export {
	createLoanApplication,
	getAllLoanApplications,
	deleteLoanApplication,
	disburseLoan,
}